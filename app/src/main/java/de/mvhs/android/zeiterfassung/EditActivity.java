package de.mvhs.android.zeiterfassung;

import android.Manifest;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.mvhs.android.zeiterfassung.db.TimeDataContract;

/**
 * Created by eugen on 06.11.16.
 */

public class EditActivity extends AppCompatActivity {
  public final static String ID_KEY = "EditItemId";
  private long _id = -1;

  private EditText _startDate;
  private EditText _startTime;
  private EditText _endDate;
  private EditText _endTime;
  private EditText _pauseTime;
  private EditText _comment;

  private Calendar _startDateTimeValue;
  private Calendar _endDateTimeValue;

  private DateFormat _dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT);
  private DateFormat _timeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT);

  // Value from the form
  private String _startValue;
  private String _endValue;
  private int _pauseTimeValue = 0;
  private String _commentValue = "";

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_grid);

    // Auslesen der ID aus Metainformationen
    _id = getIntent().getLongExtra(ID_KEY, -1);

    // Initialisieren der UI Elemente
    _startDate = (EditText) findViewById(R.id.StartDateValue);
    _startDate.setKeyListener(null);
    _startTime = (EditText) findViewById(R.id.StartTimeValue);
    _startTime.setKeyListener(null);

    _endDate = (EditText) findViewById(R.id.EndDateValue);
    _endDate.setKeyListener(null);
    _endTime = (EditText) findViewById(R.id.EndTimeValue);
    _endTime.setKeyListener(null);

    _pauseTime = (EditText) findViewById(R.id.PauseValue);
    _comment = (EditText) findViewById(R.id.CommentValue);
  }

  @Override
  protected void onStart() {
    super.onStart();

    LoadData();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_edit, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.MenuSaveEntry:
        SaveData();
       return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void LoadData(){
    // Neuer Datensatz
    if (_id == -1){
      // Set activity title
      setTitle(R.string.NewDataActivityTitle);

      _startDateTimeValue = _endDateTimeValue = Calendar.getInstance();
    }
    else
    {
      // Set activity title
      setTitle(R.string.EditDataActivityTitle);
      // Get values from database
      // Vorhandener Datensatz
      Uri dataUri =
              ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _id);

      Cursor data = getContentResolver().query(dataUri, null, null, null, null);

      // Daten lesen
      if (data.moveToFirst()) {
        // Startzeit/ endzeit auslesen
        _startValue = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.START));
        _endValue = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.END));
        _pauseTimeValue = data.getInt(data.getColumnIndex(TimeDataContract.TimeData.Columns.PAUSE));
        _commentValue = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.COMMENT));

        try {
          _startDateTimeValue = TimeDataContract.Converter.parseFromDb(_startValue);
          _endDateTimeValue = TimeDataContract.Converter.parseFromDb(_endValue);
        } catch (ParseException e) {
          e.printStackTrace();
        }
      }
      data.close();
    }

    // Ausgabe an der Oberfläche
    _startDate.setText(_dateFormatter.format(_startDateTimeValue.getTime()));
    _startTime.setText(_timeFormatter.format(_startDateTimeValue.getTime()));

    _endDate.setText(_dateFormatter.format(_endDateTimeValue.getTime()));
    _endTime.setText(_timeFormatter.format(_endDateTimeValue.getTime()));
    _pauseTime.setText(String.valueOf(_pauseTimeValue));
    _comment.setText(_commentValue);
  }

  private void SaveData(){

    // find out values
    String pauseTimeString = _pauseTime.getText().toString();
    if(pauseTimeString.matches("")) pauseTimeString = "0";

    _pauseTimeValue = Integer.parseInt(pauseTimeString);
    _commentValue = _comment.getText().toString();

    // Save values as parameters
    ContentValues values = new ContentValues();
    values.put(TimeDataContract.TimeData.Columns.PAUSE, _pauseTimeValue);
    values.put(TimeDataContract.TimeData.Columns.COMMENT, _commentValue);

    if(_id == -1)
    {
      // Create new entry
      // Add start and end datetime values to parameters (dates are already assigned, so not needed to loas them from form)
      values.put(TimeDataContract.TimeData.Columns.START, TimeDataContract.Converter.formatForDb(_startDateTimeValue));
      values.put(TimeDataContract.TimeData.Columns.END, TimeDataContract.Converter.formatForDb(_endDateTimeValue));
      Uri insertedEntryUri = getContentResolver().insert(TimeDataContract.TimeData.CONTENT_URI, values);
      // Set id of a newly inserted entry
      _id = ContentUris.parseId(insertedEntryUri);
      // Display message saying that the save has been done succesfully#
      DisplayMessage(this.getString(R.string.NewDataActivityTitle), this.getString(R.string.NewEntrySavedSuccesfully));
      // Set activity title
      setTitle(R.string.EditDataActivityTitle);
    }
    else
    {
      // Update existing entry
      Uri updateUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _id);
      getContentResolver().update(updateUri, values, null, null);
      // Display message saying that the save has been done succesfully
      DisplayMessage(this.getString(R.string.EditDataActivityTitle), this.getString(R.string.ExistingEntrySavedSuccesfully));
    }
  }

  private void DisplayMessage(String title, String message)
  {
    AlertDialog.Builder builder = new AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setNeutralButton(this.getString(R.string.OkButton),
                    new DialogInterface.OnClickListener() {
                      public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                      }
                    });
    ;

    builder.create().show();
  }
}
